/**
 * @param {Float} value 
 * @return {String}
 * Retorna uma string com o valor resebido formatado em reais. Ex 10000 => R$ 10.000,00
 */
const getFormattedValue = value => {
	let valueString = (value).toLocaleString('pt-BR');
	valueString = valueString.split(',');
	if (!valueString[1]) {
		return `R$ ${valueString[0]},00`;
	} else if (valueString[1].length === 1) {
		return `R$ ${valueString[0]},${valueString[1]}0`;
	}
	return `R$ ${valueString[0]},${valueString[1]}`;
}

export {getFormattedValue};