import Vue from 'vue'
import Vuex from 'vuex'
import cars from '@/cars.js'

Vue.use(Vuex)

export default new Vuex.Store({
	state: {
		modal: {
			active: false,
			content: null,
			title: null,
		},
		cars: {
			cars,
			display: 'inline',
			lowestValue: 0,
			highestValue: 999999
		}
	},
	mutations: {
		closeModal(state) {
			state.modal.active = false;
		},
		openModal(state, payload) {
			state.modal.title = payload.title;
			state.modal.content = payload.content;
			state.modal.active = true;
		},
		changeDisplay(state, display) {
			state.cars.display = display;
		},
		setCarValues(state) {
			state.cars.highestValue = Math.max.apply(Math, state.cars.cars.map(car => car.value));
			state.cars.lowestValue = Math.min.apply(Math, state.cars.cars.map(car => car.value));
		},
		setLowestValue(state, value) {
			state.cars.lowestValue = value;
		},
		setHighestValue(state, value) {
			state.cars.highestValue = value;
		}
	},
})
